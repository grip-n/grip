import { createVuetify, ThemeDefinition } from "vuetify";
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";

export default defineNuxtPlugin((nuxtApp) => {
  const customTheme: ThemeDefinition = {
    colors: {
      // background: '#333333',
      surface: '#454545',
      "on-surface": '#E9E9E9',
      primary: '#5DA797',
      "on-primary": '#E9E9E9',
      secondary: '#6AC1B7',
      error: '#FC9D9D',
      info: '#6AC1B7',
      success: '#60CAAD',
      warning: '#FF9800',
    }
  };

  const vuetify = createVuetify({
    components,
    directives,
    theme: {
      defaultTheme: 'customTheme',
      themes: {
        customTheme
      },

    }
  });

  nuxtApp.vueApp.use(vuetify);
});
