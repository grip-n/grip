// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr: false,
  target: "static",

  runtimeConfig: {
    public: {
    },
  },

  css: ['vuetify/lib/styles/main.sass', "@mdi/font/css/materialdesignicons.css"],

  build: {
    transpile: ["vuetify"],
  },
  vite: {
    define: {
      "process.env.DEBUG": false,
    },
  },

  app: {
    head: {
      title: "kakeibo",
      meta: [
        { name: "viewport", content: "width=device-width, initial-scale=1" },
      ],
      link: [{ rel: "icon", type: "image/png", href: "/favicon.ico" }],
    },
  },

  alias: {
    yup: "yup/lib/index.js",
  },

  buildModules: ["@pinia/nuxt"],
});
