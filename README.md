# 開発環境セットアップ

## mysql マイグレーション

```bash
docker-compose exec server bash
cd go_grpc/db
sql-migrate up
```

## grpc server

```bash
docker-compose exec server bash
cd go_grpc/server
go run main.go
```

## grpc gateway

```bash
docker-compose exec server bash
cd go_grpc/gateway
go run main.go
```

## nuxt

```bash
docker-compose exec nuxt sh
yarn install
yarn dev
```
