package main

import (
	"context"
	"encoding/csv"
    "log"
    "os"
    "io"
	// "time"
	"fmt"
	"strconv"
	"database/sql"
	// "github.com/volatiletech/sqlboiler/v4/boil"
	// "github.com/volatiletech/sqlboiler/v4/queries/qm"

	pb "grip-grpc/pb"

	grpc "google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	models "grip-grpc/sql/models"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	db, _ := sql.Open("mysql", "root:root@tcp(mysql:3306)/grip?parseTime=true")

	conn, err := grpc.Dial("server:8080", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewAccountBookApiClient(conn)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// アプリのデータを読み込む
    f, err := os.Open("../data/test.csv")
    if err != nil {
        log.Fatal(err)
    }

    r := csv.NewReader(f)

	// 対象ユーザーの収入種別を取得
	mi, err :=models.Incomes(models.IncomeWhere.UserGroupID.EQ(1)).All(ctx,db)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	// 対象ユーザーの支出種別を取得
	me, err :=models.Expenses(models.ExpenseWhere.UserGroupID.EQ(1)).All(ctx,db)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	// 対象ユーザーの資産種別を取得
	ma, err :=models.Assets(models.AssetWhere.UserID.EQ(1)).All(ctx,db)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	var transactionItemNameList [13][2]string = [13][2]string{{"現金","所持金"},
						{"UFJ","三菱UFJ銀行"},
						{"edy","楽天Edy（おサイフケータイ）"},
						{"衣服","服飾雑貨"},
						{"家電","家具・家電"},
						{"歯医者","家具・家電"},
						{"歯医者","医療"},
						{"携帯","通信費"},
						{"お酒","酒"},
						{"医療費","医療"},
						{"家具","家具・家電"},
						{"お昼代","食費"},
						{"ToMeCard","ANA JCB"}}

	a := []*pb.AccountBook{}
	// アプリの読み込んだデータを1行ずつ読み込み登録されている種別に一致しているIDを探す
	// 登録されている種別がない場合はその他で登録
    for i := 0; ; i++ {
        record, err := r.Read()
        if err == io.EOF {
            break
        }
        if err != nil {
            log.Fatal(err)
        }
        if i == 0 {
            continue
        }

		transactionTypeName := record[6]
		var transactionItemId int64
		var transactionItemGroupId int64
		var transactionType int32
		var assetId int64
		var assetGroupId int64
		var transactionItemName = record[2]
		// ユーザー間での相違を変換（とりあえず固定。。。）
		if "光熱費" == transactionItemName {
			if len(record[4]) <= 2 {
				transactionItemName = record[4] + "代"
			} else {
				transactionItemName = record[4]
			}
		} else {
			for _, x := range transactionItemNameList {
				if x[0] == transactionItemName {
					transactionItemName = x[1]
					break
				}
			}
		}
		// 取引区分を判定し、突合対象となるマスタデータを決定する
		if transactionTypeName == "収入" {
			transactionType = 1
			for _, v := range mi {
				// 突合の結果、一致しているデータが有ればidを設定する
				if v.Name == transactionItemName {
					transactionItemId = v.ID
					if v.IncomeGroupID.Valid == true {
						transactionItemGroupId = v.IncomeGroupID.Int64
					}
					break
				} else if v.Name == "その他" {
					transactionItemId = v.ID
					if v.IncomeGroupID.Valid == true {
						transactionItemGroupId = v.IncomeGroupID.Int64
					}
				}
			}
		} else if transactionTypeName == "支出" {
			transactionType = 2
			for _, v := range me {
				// 突合の結果、一致しているデータが有ればidを設定する
				if v.Name == transactionItemName {
					transactionItemId = v.ID
					if v.ExpenseGroupID.Valid == true {
						transactionItemGroupId = v.ExpenseGroupID.Int64
					}
					break
				} else if v.Name == "その他" {
					transactionItemId = v.ID
					if v.ExpenseGroupID.Valid == true {
						transactionItemGroupId = v.ExpenseGroupID.Int64
					}
				}
			}
		} else if transactionTypeName == "引き出し" {
			transactionType = 3
			for _, v := range ma {
				// 突合の結果、一致しているデータが有ればidを設定する
				if v.Name == transactionItemName {
					transactionItemId = v.ID
					transactionItemGroupId = v.AssetGroupID
					break
				} else if v.Name == "その他" {
					transactionItemId = v.ID
					transactionItemGroupId = v.AssetGroupID
				}
			}
		} else {
			fmt.Printf(transactionTypeName)
			log.Fatalf("failed transactionType")
		}

		var assetName = record[1]
		for _, x := range transactionItemNameList {
			if x[0] == assetName {
				assetName = x[1]
				break
			}
		}
		for _, v := range ma {
			// 突合の結果、一致しているデータが有ればidを設定する
			if v.Name == assetName {
				assetId = v.ID
				assetGroupId = v.AssetGroupID
				break
			} else if v.Name == "その他" {
				assetId = v.ID
				assetGroupId = v.AssetGroupID
			}
		}
		// 上記で突合して得られたidで登録するためのデータを作成する
		amount, _ := strconv.Atoi(record[5])
		accountBook := &pb.AccountBook{UserId: 1, AssetGroupId: assetGroupId, AssetId: assetId, TransactionType: transactionType, TransactionItemGroupId: transactionItemGroupId, TransactionItemId: transactionItemId, TransactionDate: record[0], Content: record[4], Amount: int64(amount)}

        a = append(a, accountBook)
    }

	// 登録用メソッドを呼び出す
	result, err := c.PutAccountBooks(ctx, &pb.AccountBooksRequest{AccountBook: a})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Printf("Greeting: %s", result.GetAccountBook())
}
