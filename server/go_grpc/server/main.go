package main

import (
	"log"
	"net"
	"time"
	// "fmt"

	pb "grip-grpc/pb"
	controller "grip-grpc/controller"
	"google.golang.org/grpc"
)

func init() {
    jst, err := time.LoadLocation("Asia/Tokyo")
    if err != nil {
        panic(err)
    }
    time.Local = jst
}

// サーバーメイン処理
func main() {
	lis, err := net.Listen("tcp", "server:8080")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()

	server1 := controller.UserServer{}
	pb.RegisterUserApiServer(s, &server1)

	server2 := controller.AccountBookServer{}
	pb.RegisterAccountBookApiServer(s, &server2)

	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
