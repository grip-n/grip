package controller

import (
	"context"
	"log"
	"time"
	// "fmt"
	"database/sql"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"

	pb "grip-grpc/pb"

	models "grip-grpc/sql/models"

	_ "github.com/go-sql-driver/mysql"

)

type AccountBookServer struct {
    pb.UnimplementedAccountBookApiServer
}

func (s *AccountBookServer) PutAccountBook(ctx context.Context, in *pb.AccountBookRequest) (*pb.AccountBookReply, error) {
	db, _ := sql.Open("mysql", "root:root@tcp(mysql:3306)/grip")

	input := in.GetAccountBook()
	var accountBook models.AccountBook
	accountBook.UserID = input.GetUserId()
	accountBook.AssetGroupID = input.GetAssetGroupId()
	accountBook.AssetID = input.GetAssetId()
	accountBook.TransactionType = int8(input.GetTransactionType())
	tGroupId := input.GetTransactionItemGroupId()
	if tGroupId != 0 {
		accountBook.TransactionItemGroupID = null.NewInt64(tGroupId,true)
	}
	accountBook.TransactionItemID = input.GetTransactionItemId()
	var transactiondateString string
	if len(input.GetTransactionDate()) <= 10 {
		transactiondateString = input.GetTransactionDate() + " 00:00:00"
	} else {
		transactiondateString = input.GetTransactionDate()
	}
	transactiondate, err := time.ParseInLocation("2006/01/02 15:04:05", transactiondateString, time.Local)
	// if err != nil {
	// 	transactiondate, _ = time.Parse("2006/01/02", input.GetTransactionDate())
	// }
	accountBook.TransactionDate = transactiondate
	content := input.GetContent()
	if len(content) != 0 {
		accountBook.Content = null.NewString(content,true)
	}
	accountBook.Amount = input.GetAmount()

	err = accountBook.Insert(ctx, db, boil.Infer())
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}


	log.Printf("Received: %v", in.GetAccountBook())
	return &pb.AccountBookReply{AccountBook:in.GetAccountBook()}, nil
}

func (s *AccountBookServer) PutAccountBooks(ctx context.Context, in *pb.AccountBooksRequest) (*pb.AccountBooksReply, error) {
	db, _ := sql.Open("mysql", "root:root@tcp(mysql:3306)/grip")

	for _ , input := range in.GetAccountBook() {
		var accountBook models.AccountBook
		accountBook.UserID = input.GetUserId()
		accountBook.AssetGroupID = input.GetAssetGroupId()
		accountBook.AssetID = input.GetAssetId()
		accountBook.TransactionType = int8(input.GetTransactionType())
		tGroupId := input.GetTransactionItemGroupId()
		if tGroupId != 0 {
			accountBook.TransactionItemGroupID = null.NewInt64(tGroupId,true)
		}
		accountBook.TransactionItemID = input.GetTransactionItemId()
		var transactiondateString string
		if len(input.GetTransactionDate()) <= 10 {
			transactiondateString = input.GetTransactionDate() + " 00:00:00"
		} else {
			transactiondateString = input.GetTransactionDate()
		}
		transactiondate, err := time.ParseInLocation("2006/01/02 15:04:05", transactiondateString, time.Local)
		// if err != nil {
		// 	transactiondate, _ = time.Parse("2006/01/02", input.GetTransactionDate())
		// }
		accountBook.TransactionDate = transactiondate
		content := input.GetContent()
		if len(content) != 0 {
			accountBook.Content = null.NewString(content,true)
		}
		accountBook.Amount = input.GetAmount()

		err = accountBook.Insert(ctx, db, boil.Infer())
		if err != nil {
			log.Fatalf("failed to listen: %v", err)
		}
	}


	log.Printf("Received: %v", in.GetAccountBook())
	return &pb.AccountBooksReply{AccountBook:in.GetAccountBook()}, nil
}

func  (s *AccountBookServer) GetMonthlyBalance(ctx context.Context, in *pb.MonthlyBalanceRequest) (*pb.MonthlyBalanceReply, error) {
	db, _ := sql.Open("mysql", "root:root@tcp(mysql:3306)/grip?parseTime=true")

	// 月初と翌月初に整形
	startDate, err := time.Parse("20060102", in.GetYears() + "01")
	endDate := startDate.AddDate(0,1,0)

	// 対象ユーザーの月初<=取引日時<翌月初までのデータを取得
	type accountBookTmp struct {
		models.AccountBook `boil:",bind"`
		models.Asset `boil:",bind"`
		TransactionItemName string `boil:"transaction_item_name" json:"transaction_item_name" toml:"transaction_item_name" yaml:"transaction_item_name"`
	}
	acclist := []accountBookTmp{}
	models.AccountBooks(models.AccountBookWhere.UserID.EQ(in.GetUserId()),
									models.AccountBookWhere.TransactionDate.GTE(startDate),
									models.AccountBookWhere.TransactionDate.LT(endDate),
									qm.Select("account_books.id, account_books.user_id, account_books.asset_group_id, account_books.asset_id, assets.name as \"asset.name\", account_books.transaction_type, account_books.transaction_item_group_id, account_books.transaction_item_id, (case account_books.transaction_type when 1 then (select name from incomes where id = account_books.transaction_item_id) when 2 then (select name from expenses where id = account_books.transaction_item_id) else (select name from assets where id = account_books.transaction_item_id) END) as \"transaction_item_name\", account_books.transaction_date, account_books.content, account_books.amount"),
									qm.OrderBy("transaction_date ASC"),
									qm.InnerJoin("assets on account_books.asset_id = assets.id")).Bind(ctx,db, &acclist)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	
	var monthlyIncomeAmount int64
	var monthlyExpenseAmount int64
	var dailyIncomeAmount int64
	var dailyExpenseAmount int64
	var tmpDate string
	accountBooklist := []*pb.AccountBook{}
	dailyBalancelist := []*pb.DailyBalance{}
	// 取得したデータの月次と日次集計を取りながら出力情報を作成
	for i, v := range acclist {
		transactionDate := v.TransactionDate.Format("2006/01/02")
		if i == 0 {
			tmpDate = transactionDate
		}

		amount := v.Amount
		var content string
		if v.Content.Valid == true {
			content = v.Content.String
		}
		var transactionItemGroupId int64
		if v.TransactionItemGroupID.Valid == true {
			transactionItemGroupId = v.TransactionItemGroupID.Int64
		}
		if tmpDate == transactionDate {
			// 収入の場合、収入合計に加算する
			if v.TransactionType == 1 {
				dailyIncomeAmount += amount
				monthlyIncomeAmount += amount
			// 支出で支出元の資産がカード以外の場合、支出合計に加算する
			} else if v.TransactionType == 2 && v.AccountBook.AssetGroupID != 4 {
				dailyExpenseAmount += amount
				monthlyExpenseAmount += amount
			// 振替で支出先の資産がカードの場合、支出合計に加算する
			} else if v.TransactionType == 3 && v.TransactionItemGroupID.Int64 == 4 {
				dailyExpenseAmount += amount
				monthlyExpenseAmount += amount
			}
		} else {
			// 日付が相違する場合、日次のデータを作成
			dailyBalance := &pb.DailyBalance{Day: tmpDate, DailyIncomeAmount: dailyIncomeAmount, DailyExpenseAmount: dailyExpenseAmount, AccountBooks: accountBooklist}
			dailyBalancelist = append(dailyBalancelist, dailyBalance)
			// 日次のデータをリセット
			accountBooklist = make([]*pb.AccountBook,0)
			dailyExpenseAmount = 0
			dailyIncomeAmount = 0
			// 収入の場合、収入合計に加算する
			if v.TransactionType == 1 {
				dailyIncomeAmount = amount
				monthlyIncomeAmount += amount
			// 支出で支出元の資産がカード以外の場合、支出合計に加算する
			} else if v.TransactionType == 2 && v.AccountBook.AssetGroupID != 4 {
				dailyExpenseAmount = amount
				monthlyExpenseAmount += amount
			// 振替で支出先の資産がカードの場合、支出合計に加算する
			} else if v.TransactionType == 3 && v.TransactionItemGroupID.Int64 == 4 {
				dailyExpenseAmount = amount
				monthlyExpenseAmount += amount
			}
			tmpDate = transactionDate
		}
		// Entityの内容を出力用に変換
		AccountBook := &pb.AccountBook{Id: v.AccountBook.ID, UserId: v.AccountBook.UserID, AssetGroupId: v.AccountBook.AssetGroupID, AssetId: v.AssetID, TransactionType: int32(v.TransactionType), TransactionItemGroupId: transactionItemGroupId,TransactionItemId: v.TransactionItemID,TransactionDate: v.TransactionDate.Format("2006/01/02 15:04:05"),Content: content,Amount: amount, AssetName: v.Asset.Name, TransactionItemName: v.TransactionItemName}
		accountBooklist = append(accountBooklist,AccountBook)


		if i+1 == len(acclist) {
			dailyBalance := &pb.DailyBalance{Day: tmpDate, DailyIncomeAmount: dailyIncomeAmount, DailyExpenseAmount: dailyExpenseAmount, AccountBooks: accountBooklist}
			dailyBalancelist = append(dailyBalancelist, dailyBalance)
		}
	}

	monthlyBalance := &pb.MonthlyBalance{Month: in.GetYears(), MonthlyIncomeAmount: monthlyIncomeAmount, MonthlyExpenseAmount: monthlyExpenseAmount, Dailybalance: dailyBalancelist}
	return &pb.MonthlyBalanceReply{Monthlybalance:monthlyBalance}, nil
}
