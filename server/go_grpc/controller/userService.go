package controller

import (
	"context"
	"log"
	"database/sql"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"fmt"
	"github.com/volatiletech/null/v8"
	"time"
	"errors"

	pb "grip-grpc/pb"

	models "grip-grpc/sql/models"

	coma "grip-grpc/com"

	_ "github.com/go-sql-driver/mysql"
)



type UserServer struct {
	pb.UnimplementedUserApiServer
}

func (s *UserServer) Login(ctx context.Context, in *pb.UserRequest) (*pb.UserReply, error) {
	db, _ := sql.Open("mysql", "root:root@tcp(mysql:3306)/grip?parseTime=true")

	u,_ := models.Users(models.UserWhere.Email.EQ(in.GetUser().GetEmail())).One(ctx,db)
	if u == nil {
		err := errors.New("一致するユーザーが存在しません。")
		fmt.Println(err)
		return nil, err
	}
	err := coma.CompareHashAndPassword(u.Password,in.GetUser().GetPassword())
	
	if err != nil {
		fmt.Println("パスワードが一致しませんでした。：", err)
		return nil, err
	}

	
	log.Printf("Success!!")
	return &pb.UserReply{User:pbConvert(u)}, nil

}

func (s *UserServer) PutUsers(ctx context.Context, in *pb.UsersRequest) (*pb.UsersReply, error) {
	db, _ := sql.Open("mysql", "root:root@tcp(mysql:3306)/grip?parseTime=true")


	for _ , input := range in.GetUser() {
		var user models.User
		u,_ := models.Users(models.UserWhere.Email.EQ(input.GetEmail())).One(ctx,db)
		if u != nil {
			err := errors.New("ご使用のメールアドレスは既に登録されています。")
			fmt.Println(err)
			return nil, err
		}
		user.Name = input.GetName()
		user.UserGroupID = input.GetUserGroupId()
		fmt.Println(input.GetUserGroupId())
		user.Email = input.GetEmail()
		fmt.Println(input.GetEmail())
		pass := input.GetPassword()
		user.Password, _ = coma.PasswordEncrypt(pass)
		bTime, _ := time.Parse("2006-01-02", input.GetBirthday())
		user.Birthday = null.NewTime(bTime, true)
		// user.Age = int(input.GetAge())
		// user.AuthID = null.NewInt(2, true)

		// fmt.Println(user)
		// fmt.Println(db)
		err := user.Insert(ctx, db, boil.Infer())
		if err != nil {
			log.Fatalf("failed to listen: %v", err)
		}
	}


	log.Printf("Received: %v", in.GetUser())
	return &pb.UsersReply{User:in.GetUser()}, nil
}

func (s *UserServer) GetUsers(ctx context.Context, in *pb.IdRequest) (*pb.UsersReply, error) {
	db, _ := sql.Open("mysql", "root:root@tcp(mysql:3306)/grip?parseTime=true")

	var m *models.User
	var err error
	userlist := []*pb.User{}
	for _ , input := range in.GetId() {

		// fmt.Println(user)
		fmt.Println(input)
		m, err = models.FindUser(ctx, db, input)
		if err != nil {
			log.Fatalf("failed to listen: %v", err)
		}
		// fmt.Println(input)
		userlist = append(userlist,pbConvert(m))
	}

	log.Printf("Received: %v", m)

	u,_ := models.Users(models.UserWhere.ID.EQ(1),qm.Load("UserGroup")).One(ctx,db)
	log.Printf("Received: %v", u)
	log.Printf("Received: %v", u.R.UserGroup)
	log.Printf("Received: %v", u.R.UserGroup.R.Users[0])

	type userMember struct {
		models.User `boil:",bind"`
		models.UserGroup `boil:",bind"`
	}
	var mem userMember
	// SQL試し1
	models.Users(models.UserWhere.ID.EQ(1),
				qm.Select("users.*, user_groups.*"),
				qm.InnerJoin("user_groups on user_groups.id = users.user_group_id")).Bind(ctx,db, &mem)
	fmt.Printf("mem = %+v\n", mem)

	memlist := []userMember{}
	// SQL試し2
	models.Users(qm.Select("users.id as \"user.id\", users.name as \"user.name\", users.email , users.user_group_id, user_groups.id as \"user_groups.id\", user_groups.name as \"user_groups.name\""),
				qm.InnerJoin("user_groups on users.user_group_id = user_groups.id"),
				qm.OrderBy("users.id")).Bind(ctx,db, &memlist)
	fmt.Printf("mem = %+v\n", memlist)

	return &pb.UsersReply{User:userlist}, nil
}

func pbConvert(u *models.User) (*pb.User) {


	// a, err := exec.Query(u.Auth)
	// a, err := u.Auth.Query.Exec(ctx,exec)
	// log.Printf("Received: %v", u.Auth)
	// a, err := u.Auth.One(ctx, exec)
	// if err != nil {
	// 	log.Fatalf("failed to listen: %v", err)
	// }
	// log.Printf("Received: %v", u.R.GetUserGroup())
	var birthday string
	if u.Birthday.Valid == true {
		birthday = u.Birthday.Time.Format("2006/01/02")
	}
	user := &pb.User{Id: u.ID, Name: u.Name, UserGroupId: u.UserGroupID, Email: u.Email, Birthday: birthday}
	return user
}
