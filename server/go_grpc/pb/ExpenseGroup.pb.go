// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.6.1
// source: ExpenseGroup.proto

package pb

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type ExpenseGroupRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ExpenseGroup *ExpenseGroup `protobuf:"bytes,1,opt,name=expenseGroup,proto3" json:"expenseGroup,omitempty"`
}

func (x *ExpenseGroupRequest) Reset() {
	*x = ExpenseGroupRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_ExpenseGroup_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ExpenseGroupRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ExpenseGroupRequest) ProtoMessage() {}

func (x *ExpenseGroupRequest) ProtoReflect() protoreflect.Message {
	mi := &file_ExpenseGroup_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ExpenseGroupRequest.ProtoReflect.Descriptor instead.
func (*ExpenseGroupRequest) Descriptor() ([]byte, []int) {
	return file_ExpenseGroup_proto_rawDescGZIP(), []int{0}
}

func (x *ExpenseGroupRequest) GetExpenseGroup() *ExpenseGroup {
	if x != nil {
		return x.ExpenseGroup
	}
	return nil
}

type ExpenseGroupReply struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ExpenseGroup *ExpenseGroup `protobuf:"bytes,1,opt,name=expenseGroup,proto3" json:"expenseGroup,omitempty"`
}

func (x *ExpenseGroupReply) Reset() {
	*x = ExpenseGroupReply{}
	if protoimpl.UnsafeEnabled {
		mi := &file_ExpenseGroup_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ExpenseGroupReply) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ExpenseGroupReply) ProtoMessage() {}

func (x *ExpenseGroupReply) ProtoReflect() protoreflect.Message {
	mi := &file_ExpenseGroup_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ExpenseGroupReply.ProtoReflect.Descriptor instead.
func (*ExpenseGroupReply) Descriptor() ([]byte, []int) {
	return file_ExpenseGroup_proto_rawDescGZIP(), []int{1}
}

func (x *ExpenseGroupReply) GetExpenseGroup() *ExpenseGroup {
	if x != nil {
		return x.ExpenseGroup
	}
	return nil
}

type ExpenseGroup struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id          int32  `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name        string `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	UserGroupId int32  `protobuf:"varint,3,opt,name=userGroupId,proto3" json:"userGroupId,omitempty"`
}

func (x *ExpenseGroup) Reset() {
	*x = ExpenseGroup{}
	if protoimpl.UnsafeEnabled {
		mi := &file_ExpenseGroup_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ExpenseGroup) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ExpenseGroup) ProtoMessage() {}

func (x *ExpenseGroup) ProtoReflect() protoreflect.Message {
	mi := &file_ExpenseGroup_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ExpenseGroup.ProtoReflect.Descriptor instead.
func (*ExpenseGroup) Descriptor() ([]byte, []int) {
	return file_ExpenseGroup_proto_rawDescGZIP(), []int{2}
}

func (x *ExpenseGroup) GetId() int32 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *ExpenseGroup) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *ExpenseGroup) GetUserGroupId() int32 {
	if x != nil {
		return x.UserGroupId
	}
	return 0
}

var File_ExpenseGroup_proto protoreflect.FileDescriptor

var file_ExpenseGroup_proto_rawDesc = []byte{
	0x0a, 0x12, 0x45, 0x78, 0x70, 0x65, 0x6e, 0x73, 0x65, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0d, 0x67, 0x6f, 0x5f, 0x67, 0x72, 0x70, 0x63, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x22, 0x56, 0x0a, 0x13, 0x45, 0x78, 0x70, 0x65, 0x6e, 0x73, 0x65, 0x47, 0x72,
	0x6f, 0x75, 0x70, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x3f, 0x0a, 0x0c, 0x65, 0x78,
	0x70, 0x65, 0x6e, 0x73, 0x65, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b,
	0x32, 0x1b, 0x2e, 0x67, 0x6f, 0x5f, 0x67, 0x72, 0x70, 0x63, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x2e, 0x45, 0x78, 0x70, 0x65, 0x6e, 0x73, 0x65, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x52, 0x0c, 0x65,
	0x78, 0x70, 0x65, 0x6e, 0x73, 0x65, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x22, 0x54, 0x0a, 0x11, 0x45,
	0x78, 0x70, 0x65, 0x6e, 0x73, 0x65, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x52, 0x65, 0x70, 0x6c, 0x79,
	0x12, 0x3f, 0x0a, 0x0c, 0x65, 0x78, 0x70, 0x65, 0x6e, 0x73, 0x65, 0x47, 0x72, 0x6f, 0x75, 0x70,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1b, 0x2e, 0x67, 0x6f, 0x5f, 0x67, 0x72, 0x70, 0x63,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x45, 0x78, 0x70, 0x65, 0x6e, 0x73, 0x65, 0x47, 0x72,
	0x6f, 0x75, 0x70, 0x52, 0x0c, 0x65, 0x78, 0x70, 0x65, 0x6e, 0x73, 0x65, 0x47, 0x72, 0x6f, 0x75,
	0x70, 0x22, 0x54, 0x0a, 0x0c, 0x45, 0x78, 0x70, 0x65, 0x6e, 0x73, 0x65, 0x47, 0x72, 0x6f, 0x75,
	0x70, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52, 0x02, 0x69,
	0x64, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x20, 0x0a, 0x0b, 0x75, 0x73, 0x65, 0x72, 0x47, 0x72, 0x6f,
	0x75, 0x70, 0x49, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x05, 0x52, 0x0b, 0x75, 0x73, 0x65, 0x72,
	0x47, 0x72, 0x6f, 0x75, 0x70, 0x49, 0x64, 0x32, 0x6c, 0x0a, 0x0f, 0x45, 0x78, 0x70, 0x65, 0x6e,
	0x73, 0x65, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x41, 0x70, 0x69, 0x12, 0x59, 0x0a, 0x0f, 0x70, 0x75,
	0x74, 0x45, 0x78, 0x70, 0x65, 0x6e, 0x73, 0x65, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x12, 0x22, 0x2e,
	0x67, 0x6f, 0x5f, 0x67, 0x72, 0x70, 0x63, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x45, 0x78,
	0x70, 0x65, 0x6e, 0x73, 0x65, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x20, 0x2e, 0x67, 0x6f, 0x5f, 0x67, 0x72, 0x70, 0x63, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x2e, 0x45, 0x78, 0x70, 0x65, 0x6e, 0x73, 0x65, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x52, 0x65,
	0x70, 0x6c, 0x79, 0x22, 0x00, 0x42, 0x06, 0x5a, 0x04, 0x2e, 0x2f, 0x70, 0x62, 0x62, 0x06, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_ExpenseGroup_proto_rawDescOnce sync.Once
	file_ExpenseGroup_proto_rawDescData = file_ExpenseGroup_proto_rawDesc
)

func file_ExpenseGroup_proto_rawDescGZIP() []byte {
	file_ExpenseGroup_proto_rawDescOnce.Do(func() {
		file_ExpenseGroup_proto_rawDescData = protoimpl.X.CompressGZIP(file_ExpenseGroup_proto_rawDescData)
	})
	return file_ExpenseGroup_proto_rawDescData
}

var file_ExpenseGroup_proto_msgTypes = make([]protoimpl.MessageInfo, 3)
var file_ExpenseGroup_proto_goTypes = []interface{}{
	(*ExpenseGroupRequest)(nil), // 0: go_grpc.proto.ExpenseGroupRequest
	(*ExpenseGroupReply)(nil),   // 1: go_grpc.proto.ExpenseGroupReply
	(*ExpenseGroup)(nil),        // 2: go_grpc.proto.ExpenseGroup
}
var file_ExpenseGroup_proto_depIdxs = []int32{
	2, // 0: go_grpc.proto.ExpenseGroupRequest.expenseGroup:type_name -> go_grpc.proto.ExpenseGroup
	2, // 1: go_grpc.proto.ExpenseGroupReply.expenseGroup:type_name -> go_grpc.proto.ExpenseGroup
	0, // 2: go_grpc.proto.ExpenseGroupApi.putExpenseGroup:input_type -> go_grpc.proto.ExpenseGroupRequest
	1, // 3: go_grpc.proto.ExpenseGroupApi.putExpenseGroup:output_type -> go_grpc.proto.ExpenseGroupReply
	3, // [3:4] is the sub-list for method output_type
	2, // [2:3] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_ExpenseGroup_proto_init() }
func file_ExpenseGroup_proto_init() {
	if File_ExpenseGroup_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_ExpenseGroup_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ExpenseGroupRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_ExpenseGroup_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ExpenseGroupReply); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_ExpenseGroup_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ExpenseGroup); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_ExpenseGroup_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   3,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_ExpenseGroup_proto_goTypes,
		DependencyIndexes: file_ExpenseGroup_proto_depIdxs,
		MessageInfos:      file_ExpenseGroup_proto_msgTypes,
	}.Build()
	File_ExpenseGroup_proto = out.File
	file_ExpenseGroup_proto_rawDesc = nil
	file_ExpenseGroup_proto_goTypes = nil
	file_ExpenseGroup_proto_depIdxs = nil
}
