
-- +migrate Up
insert into asset_groups (name,user_id) values ('現金',1);
insert into asset_groups (name,user_id) values ('銀行',1);
insert into asset_groups (name,user_id) values ('電子マネー',1);
insert into asset_groups (name,user_id) values ('クレジットカード',1);
insert into asset_groups (name,user_id) values ('その他',1);

insert into assets (name,asset_group_id,user_id) values ('所持金',1,1);
insert into assets (name,asset_group_id,user_id) values ('銀行',2,1);
insert into assets (name,asset_group_id,user_id) values ('電子マネー',3,1);
insert into assets (name,asset_group_id,user_id,settlement_account_id,closing_date,payment_date) values ('クレジットカード',4,1,3,28,27);
insert into assets (name,asset_group_id,user_id) values ('その他',5,1);

insert into incomes (name,user_group_id) values ('給料',1);
insert into incomes (name,user_group_id) values ('ボーナス',1);
insert into incomes (name,user_group_id) values ('その他',1);

insert into expense_groups (name,user_group_id) values ('光熱費',1);

insert into expenses (name,user_group_id) values ('食費',1);
insert into expenses (name,user_group_id) values ('交通費',1);
insert into expenses (name,user_group_id) values ('家具',1);
insert into expenses (name,user_group_id) values ('家電',1);
insert into expenses (name,user_group_id) values ('その他',1);
insert into expenses (name,expense_group_id,user_group_id) values ('電気代',1,1);
insert into expenses (name,expense_group_id,user_group_id) values ('ガス代',1,1);
insert into expenses (name,expense_group_id,user_group_id) values ('水道代',1,1);

-- +migrate Down
delete from account_books;
ANALYZE TABLE account_books;
ALTER TABLE account_books AUTO_INCREMENT = 1;
ANALYZE TABLE account_books;
delete from incomes;
ANALYZE TABLE incomes;
ALTER TABLE incomes AUTO_INCREMENT = 1;
ANALYZE TABLE incomes;
delete from expenses;
ANALYZE TABLE expenses;
ALTER TABLE expenses AUTO_INCREMENT = 1;
ANALYZE TABLE expenses;
delete from expense_groups;
ANALYZE TABLE expense_groups;
ALTER TABLE expense_groups AUTO_INCREMENT = 1;
ANALYZE TABLE expense_groups;
delete from assets;
ANALYZE TABLE assets;
ALTER TABLE assets AUTO_INCREMENT = 1;
ANALYZE TABLE assets;
delete from asset_groups;
ANALYZE TABLE asset_groups;
ALTER TABLE asset_groups AUTO_INCREMENT = 1;
ANALYZE TABLE asset_groups;
