
-- +migrate Up
insert into user_groups (name) values ('田中');


-- +migrate Down
delete from users;
ANALYZE TABLE users;
ALTER TABLE users AUTO_INCREMENT = 1;
ANALYZE TABLE users;
delete from user_groups;
ANALYZE TABLE user_groups;
ALTER TABLE user_groups AUTO_INCREMENT = 1;
ANALYZE TABLE user_groups;
