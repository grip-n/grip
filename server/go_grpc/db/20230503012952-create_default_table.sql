
-- +migrate Up
CREATE TABLE users(
    id bigint AUTO_INCREMENT NOT NULL,
    name varchar(255) NOT NULL,
    user_group_id bigint NOT NULL,
    email varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    birthday date,
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp,
    deleted_at timestamp,
    PRIMARY KEY (id));
    
CREATE TABLE user_groups(
    id bigint AUTO_INCREMENT NOT NULL,
    name varchar(255) NOT NULL,
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp,
    deleted_at timestamp,
    PRIMARY KEY (id));

CREATE TABLE assets(
    id bigint AUTO_INCREMENT NOT NULL,
    name varchar(255) NOT NULL,
    asset_group_id bigint NOT NULL,
    user_id bigint NOT NULL,
    settlement_account_id bigint,
    closing_date tinyint(2) ZEROFILL,
    payment_date tinyint(2) ZEROFILL,
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp,
    deleted_at timestamp,
    PRIMARY KEY (id));
    
CREATE TABLE asset_groups(
    id bigint AUTO_INCREMENT NOT NULL,
    name varchar(255) NOT NULL,
    user_id bigint NOT NULL,
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp,
    deleted_at timestamp,
    PRIMARY KEY (id));

CREATE TABLE expenses(
    id bigint AUTO_INCREMENT NOT NULL,
    name varchar(255) NOT NULL,
    expense_group_id bigint,
    user_group_id bigint NOT NULL,
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp,
    deleted_at timestamp,
    PRIMARY KEY (id));
    
CREATE TABLE expense_groups(
    id bigint AUTO_INCREMENT NOT NULL,
    name varchar(255) NOT NULL,
    user_group_id bigint NOT NULL,
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp,
    deleted_at timestamp,
    PRIMARY KEY (id));

CREATE TABLE incomes(
    id bigint AUTO_INCREMENT NOT NULL,
    name varchar(255) NOT NULL,
    income_group_id bigint,
    user_group_id bigint NOT NULL,
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp,
    deleted_at timestamp,
    PRIMARY KEY (id));
    
CREATE TABLE income_groups(
    id bigint AUTO_INCREMENT NOT NULL,
    name varchar(255) NOT NULL,
    user_group_id bigint NOT NULL,
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp,
    deleted_at timestamp,
    PRIMARY KEY (id));
    
CREATE TABLE account_books(
    id bigint AUTO_INCREMENT NOT NULL,
    user_id bigint NOT NULL,
    asset_group_id bigint NOT NULL,
    asset_id bigint NOT NULL,
    transaction_type tinyint NOT NULL,
    transaction_item_group_id bigint,
    transaction_item_id bigint NOT NULL,
    transaction_date timestamp NOT NULL,
    content varchar(255),
    amount bigint NOT NULL,
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp,
    deleted_at timestamp,
    PRIMARY KEY (id));

-- +migrate Down
DROP TABLE users;
DROP TABLE user_groups;
DROP TABLE assets;
DROP TABLE asset_groups;
DROP TABLE expenses;
DROP TABLE expense_groups;
DROP TABLE incomes;
DROP TABLE income_groups;
DROP TABLE account_books;