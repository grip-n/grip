
-- +migrate Up
ALTER TABLE users ADD FOREIGN KEY fk_user_group(user_group_id) REFERENCES user_groups(id);
ALTER TABLE assets ADD FOREIGN KEY fk_asset_group(asset_group_id) REFERENCES asset_groups(id);
ALTER TABLE assets ADD FOREIGN KEY fk_asset_user(user_id) REFERENCES users(id);
ALTER TABLE asset_groups ADD FOREIGN KEY fk_asset_groups_user(user_id) REFERENCES users(id);
ALTER TABLE expenses ADD FOREIGN KEY fk_expense_group(expense_group_id) REFERENCES expense_groups(id);
ALTER TABLE expenses ADD FOREIGN KEY fk_expense_user(user_group_id) REFERENCES user_groups(id);
ALTER TABLE expense_groups ADD FOREIGN KEY fk_expense_groups_user(user_group_id) REFERENCES user_groups(id);
ALTER TABLE incomes ADD FOREIGN KEY fk_income_group(income_group_id) REFERENCES income_groups(id);
ALTER TABLE incomes ADD FOREIGN KEY fk_income_user(user_group_id) REFERENCES user_groups(id);
ALTER TABLE income_groups ADD FOREIGN KEY fk_income_groups_user(user_group_id) REFERENCES user_groups(id);
ALTER TABLE account_books ADD FOREIGN KEY fk_account_books_user(user_id) REFERENCES users(id);
ALTER TABLE account_books ADD FOREIGN KEY fk_account_books_asset(asset_id) REFERENCES assets(id);
ALTER TABLE account_books ADD FOREIGN KEY fk_account_books_asset_group(asset_group_id) REFERENCES asset_groups(id);

-- +migrate Down
 ALTER TABLE users DROP FOREIGN KEY users_ibfk_1;
 ALTER TABLE assets DROP FOREIGN KEY assets_ibfk_1;
 ALTER TABLE assets DROP FOREIGN KEY assets_ibfk_2;
 ALTER TABLE asset_groups DROP FOREIGN KEY asset_groups_ibfk_1;
 ALTER TABLE expenses DROP FOREIGN KEY expenses_ibfk_1;
 ALTER TABLE expenses DROP FOREIGN KEY expenses_ibfk_2;
 ALTER TABLE expense_groups DROP FOREIGN KEY expense_groups_ibfk_1;
 ALTER TABLE incomes DROP FOREIGN KEY incomes_ibfk_1;
 ALTER TABLE incomes DROP FOREIGN KEY incomes_ibfk_2;
 ALTER TABLE income_groups DROP FOREIGN KEY income_groups_ibfk_1;
 ALTER TABLE account_books DROP FOREIGN KEY account_books_ibfk_1;
 ALTER TABLE account_books DROP FOREIGN KEY account_books_ibfk_2;
 ALTER TABLE account_books DROP FOREIGN KEY account_books_ibfk_3;